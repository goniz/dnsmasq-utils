#!/usr/bin/python2

import utils
import time
import datetime

class DnsmasqLeaseEntry(object):
	def __init__(self, line = None):
		self.line = line
		self.expires = ''
		self.mac = ''
		self.ip = ''
		self.hostname = ''
		self.client_id = ''
		if line:
			self.parse(line)

	@property
	def time_left(self):
		now = datetime.datetime.now()
		return ':'.join(str(self.expires - now).split(':')[:2])

	def parse(self, line):
		fields = map(str.strip, line.split(' '))

		# the first field is a ctime timestamp of the expire date
		self.expires = utils.get_timestamp(fields[0])

		# the second field is the mac address
		if not utils.is_mac(fields[1]):
			raise AddressError("'%s': Not a MAC address" % (fields[1], ))
		self.mac = fields[1]

		# the third field is the given ip address
		if not utils.is_ip(fields[2]):
			raise AddressError("'%s': Not an IP address" % (fields[2]), )
		self.ip = fields[2]

		# the last field is the client id
		self.client_id = fields[-1]

		# the forth field is the hostname of the device
		# i've done it this way in case dnsmasq supports whitespaces in the hostname
		self.hostname = ' '.join(fields[3:-1])


	def __str__(self):
		s = self.expires
		s = utils.append_comma(s, self.mac, True)
		s = utils.append_comma(s, self.ip, True)
		s = utils.append_comma(s, self.hostname, True)
		s = utils.append_comma(s, self.client_id, True)
		return s

class DnsmasqLeases(object):
	def __init__(self, filename):
		self.filename = filename
		self.reload()

	def reload(self):
		self.content = open(self.filename, 'rb').read().split('\n')
		self.content = filter(None, self.content)
		self.entries = self.parse()

	def parse(self):
		entries = []
		for line in self.content:
			entry = DnsmasqLeaseEntry(line=line)
			entries.append(entry)
		return entries

	def __str__(self):
		s = map(str, self.entries)
		return '\n'.join(s).strip()
