#!/usr/bin/python2

from dnsmasq_utils.configfile import DnsmasqConfig
from dnsmasq_utils.leasesfile import DnsmasqLeases

def main():
	a = DnsmasqConfig('dnsmasq.conf')
	b = DnsmasqLeases('dnsmasq.leases')
	print 'Config:'
	for entry in a.entries:
		print entry

	print 'Leases:'
	print b

if '__main__' == __name__:
	main()

